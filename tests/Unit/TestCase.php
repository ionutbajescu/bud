<?php

namespace Tests;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

class TestCase extends \PHPUnit\Framework\TestCase
{
    /**
     * @return \PHPUnit\Framework\MockObject\MockObject
     */
    public function mockWithCreateToken(array $additionalRespones = []): Client
    {
        $mock = new MockHandler($additionalRespones);

        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        return $client;
    }

    protected function getTokenResponse(): Response
    {
        return new Response(200, [], json_encode([
            'access_token' => 'e31a726c4b90462ccb7619e1b51f3d0068bf8006',
            'expires_in' => 99999999999,
            'token_type' => 'Bearer',
            'scope' => 'TheForce'
        ]));
    }
}