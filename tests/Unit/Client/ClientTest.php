<?php

namespace Tests\Client;

use Deathstar\Client\Exceptions\InvalidJsonResponseException;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;

class ClientTest extends TestCase
{
    public function testReturnsTokenIfCreateTokenRequestIsSuccessful()
    {
        $stub = $this->mockWithCreateToken([$this->getTokenResponse()]);

        $client = new \Deathstar\Client\Client($stub);
        $this->assertInstanceOf(
            \Deathstar\Client\Token::class,
            $client->createToken()
        );
    }

    public function testThrowsExceptionIfCreateTokenResponseIsInvalid()
    {
        $this->expectException(InvalidJsonResponseException::class);
        $stub = $this->mockWithCreateToken([
            new Response(200, [], 'this is not valid json"')
        ]);

        $client = new \Deathstar\Client\Client($stub);
        $client->createToken();
    }

    public function testDestroyReactorReturnsCorrectStatusCode()
    {
        $stub = $this->mockWithCreateToken([
            $this->getTokenResponse(),
            new Response(204)
        ]);

        $client = new \Deathstar\Client\Client($stub);
        $this->assertEquals(
            $client->deleteReactorExhaust(1)->getStatusCode(),
            204
        );
    }

    public function testGetPrisonerDecodesResponse()
    {
        $stub = $this->mockWithCreateToken([
            $this->getTokenResponse(),
            new Response(200, [], json_encode([
                'cell' => "01000011 01100101 01101100 01101100 00100000 00110010 00110001 00111000 00110111",
                'block' => "01000100 01100101 01110100 01100101 01101110 01110100 01101001 01101111 01101110
00100000 01000010 01101100 01101111 01100011 01101011 00100000 01000001 01000001 00101101 00110010 00110011 00101100"
            ]))
        ]);

        $client = new \Deathstar\Client\Client($stub);
        $prisoner = $client->getPrisoner('leia');
        $this->assertEquals('Cell 2187', $prisoner->cell);
        $this->assertEquals('Detentio Block AA-23,', $prisoner->block);
    }
}
