.PHONY: docker

docker:
	docker build -t bud .

vendor: composer.json docker
	docker run --rm -v ${PWD}:/app composer install

