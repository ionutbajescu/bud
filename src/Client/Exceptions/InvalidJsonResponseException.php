<?php
declare(strict_types=1);

namespace Deathstar\Client\Exceptions;

class InvalidJsonResponseException extends \Exception
{
    public function __construct(string $response)
    {
        // Using var_export allows us to see if the value is null / false
        parent::__construct(sprintf(
            "The response returned by the API is not a valid json document: %s.",
            var_export($response, true)
        ));
    }
}