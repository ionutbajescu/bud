<?php


namespace Deathstar\Client;


class DroidSpeakDecoder
{
    /**
     * @todo Make this work with other charsets than ASCII in the future.
     */
    public function decode(string $encoded): string {
        $result = '';

        foreach (explode(' ', $encoded) as $binaryCharacter) {
            $result .= chr(bindec($binaryCharacter));
        }

        return $result;
    }
}