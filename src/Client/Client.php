<?php
declare(strict_types=1);

namespace Deathstar\Client;

use Deathstar\Client\Exceptions\InvalidJsonResponseException;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

class Client
{
    /**
     * @var ClientInterface
     */
    private $httpClient;

    public function __construct(ClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    static public function fromGuzzle(string $baseUrl, string $cert = '/path/to/cert')
    {
        return new static(new GuzzleClient([
            'baseUrl' => $baseUrl,
            'cert' => $cert
        ]));
    }

    /**
     * @throws InvalidJsonResponseException
     * @throws GuzzleException
     */
    public function createToken(): Token
    {
        $parsed = $this->parseJsonResponse($this->httpClient->request('POST', 'token', [
            'grant_type' => 'client_credentials'
        ]));

        return new Token($parsed['access_token'], $parsed['expires_in'], $parsed['token_type'], $parsed['scope']);
    }

    public function deleteReactorExhaust(int $exhaustId, int $torpedoes = 2)
    {
        return $this->requestWithToken('DELETE', "/reactor/exhaust/$exhaustId", [
            'headers' => [
                'x-torpedoes' => $torpedoes
            ]
        ]);
    }

    public function getPrisoner(string $prisonerName): Prisoner
    {
        $response = $this->requestWithToken('GET', "/prisoner/$prisonerName");
        $parsed = $this->parseJsonResponse($response);

        $droidSpeakDecoder = new DroidSpeakDecoder();
        return new Prisoner(
            $droidSpeakDecoder->decode($parsed['cell']),
            $droidSpeakDecoder->decode($parsed['block'])
        );
    }

    protected function requestWithToken(string $method, string $path, array $options = [])
    {
        if (!isset($options['headers'])) {
            $options['headers'] = [];
        }

        $options['headers']['Authorization'] = "Bearer {$this->createToken()->accessToken}";

        return $this->httpClient->request($method, $path, $options);
    }


    /**
     * @throws GuzzleException
     * @throws InvalidJsonResponseException
     */
    protected function parseJsonResponse(ResponseInterface $response)
    {
        $body = $response->getBody()->__toString();
        $parsed = json_decode($body, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new InvalidJsonResponseException($body);
        }

        return $parsed;
    }
}
