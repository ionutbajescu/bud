<?php
declare(strict_types=1);

namespace Deathstar\Client;


class Token
{
    /**
     * @var string
     */
    public $accessToken;

    /**
     * @var integer
     */
    public $expiresIn;

    /**
     * @var string
     */
    public $tokenType;

    /**
     * @var string
     */
    public $scope;

    public function __construct(string $accessToken, int $expiresIn, string $tokenType, string $scope)
    {
        $this->accessToken = $accessToken;
        $this->expiresIn = $expiresIn;
        $this->tokenType = $tokenType;
        $this->scope = $scope;
    }
}