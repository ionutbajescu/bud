<?php


namespace Deathstar\Client;

class Prisoner
{
    /**
     * @var string
     */
    public $cell;

    /**
     * @var string
     */
    public $block;

    public function __construct(string $cell, string $block)
    {
        $this->cell = $cell;
        $this->block = $block;
    }
}