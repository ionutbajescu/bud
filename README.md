# Bud Technical Test
    object(Deathstar\Client\Prisoner)#83 (2) {
      ["cell"]=>
      string(9) "Cell 2187"
      ["block"]=>
      string(21) "Detentio Block AA-23,"
    }


## Design Patterns
I think it is common for people in these documents to present a list of design patterns that they have used in crafting their application, and wear these design patterns as a badge of honour. The higher the number of design patterns, the better the developer.

No such thing in this document. I have been aiming to keep things as simple as possible in this test.

## /Token
Quite a nitpicky comment, but since the /Token endpoint starts with an uppercase letter while the others start with lowercase (as expected for most APIs not written in Java or other languages that use uppercase symbols more commonly), I've assumed that to be a typo and used /token instead in my client. If that's not the case, it should only take a single character change in Client to fix it.

I wanted to put this here since this kind of assumptions that I have (and we all do) make on a daily basis create the proverbial "works on my machine" problems.

## 'grant_type' => 'client_credentials'
I've hardcoded the grant_type to client_credentials now for demonstration purposes, but assuming that API supports a higher level of features this could be extended to be user-provided and the user of the API client could choose the level of grant permissions that they need.

## x-torpedoes:2
While the behaviour of this header is quite unclear, as it stands I believe it should be moved in the request body rather than being in the headers.

## Decoding droidspeak
As with decoding any binary to a string, you must assume a charset in order to display the characters. In my code I have assumed ASCII (since it can be done with a simple chr call) but the function could be made to work with other charsets as well.